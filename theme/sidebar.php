<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Colegio_Virtual
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">

	<section id="sidebar-ad-top" class="widget">
		<div class="sidebar-ad text-center">
			<img src="https://placehold.it/300x600">
		</div>
	</section>

	<?php dynamic_sidebar( 'sidebar-1' ); ?>

	<section id="sidebar-ad-bottom" class="widget">
		<div class="sidebar-ad text-center">
			<img src="https://placehold.it/300x600">
		</div>
	</section>

</aside><!-- #secondary -->

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Colegio_Virtual
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : ?>

				<div class="row">
					<div class="col-md-9">
						<?php the_post(); get_template_part( 'template-parts/content', 'page' ); ?>
					</div>

					<div class="col-md-3">
						<?php get_sidebar(); ?>
					</div>
				</div>


			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

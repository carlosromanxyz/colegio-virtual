<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Colegio_Virtual
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="row">
				<div class="col-md-9">

					<?php get_template_part( 'template-parts/content', get_post_type() ); ?>

					<?php the_post_navigation(); ?>

				</div>

				<div class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>

		<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

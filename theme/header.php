<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Colegio_Virtual
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'colegiovirtual' ); ?></a>

	<header id="header-top" class="site-header">
		<div class="container">
			<div class="pull-left">
				<ul class="list-unstyled">
					<li><i class="fa fa-envelope"></i><span><a href="mailto:contacto@colegiovirtual.cl"><?php _e( 'contacto@colegiovirtual.cl', 'colegiovirtual' ); ?></a></span></li>
				</ul>
			</div>
			<div class="pull-right">
				<ul class="list-unstyled">
					<li><i class="fa fa-user"></i><span><a href="#"><?php _e( 'Mi cuenta', 'colegiovirtual' ); ?></a></span></li>
				</ul>
			</div>
		</div>
	</header>

	<header id="masthead" class="site-header">

		<nav id="site-navigation" class="navbar navbar-theme">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only"><?php _e('Menú de navegación', 'colegiovirtual'); ?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<?php bloginfo('name') ?>
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<?php

						/**
						* Displays a navigation menu
						* @param array $args Arguments
						*/
						$args = array(
							'theme_location' => 'menu-1',
							'menu' => '',
							'container' => '',
							'container_class' => 'menu-{menu-slug}-container',
							'container_id' => '',
							'menu_class' => 'menu',
							'menu_id' => '',
							'echo' => true,
							'fallback_cb' => 'wp_page_menu',
							'before' => '',
							'after' => '',
							'link_before' => '',
							'link_after' => '',
							'items_wrap' => '%3$s',
							'depth' => 0,
							'walker' => ''
						);
					
						wp_nav_menu( $args ); ?>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>

	</header><!-- #masthead -->

	<div id="content" class="site-content container container-with-background">

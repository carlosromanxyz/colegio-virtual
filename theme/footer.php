<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Colegio_Virtual
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info container">
			<div class="row">
				<div class="col-md-4">
					<div class="footer-widget">
						<h2 class="widget-title"><?php _e( 'Widget Title', 'colegiovirtual' ); ?></h2>
						<ul class="list-unstyled">
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="footer-widget">
						<h2 class="widget-title"><?php _e( 'Widget Title', 'colegiovirtual' ); ?></h2>
						<ul class="list-unstyled">
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="footer-widget">
						<h2 class="widget-title"><?php _e( 'Widget Title', 'colegiovirtual' ); ?></h2>
						<ul class="list-unstyled">
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

	<footer id="footer-bottom">
		<p class="text-center">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>. <?php _e( 'Derechos reservados', 'colegiovirtual' ); ?></p>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

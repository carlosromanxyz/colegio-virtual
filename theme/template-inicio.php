<?php
/**
 * Template name: Inicio
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Colegio_Virtual
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="row">
					<div class="col-md-9">

						<!-- Carrusel -->
						<div id="carousel-home" class="carousel slide" data-ride="carousel">

							<!-- Wrapper for slides -->
							<div class="carousel-inner" role="listbox">
								<div class="item active">
									<img src="http://lorempixel.com/1024/500/people/1" alt="...">
									<div class="carousel-caption">
										<h3 class="text-left">Lorem ipsum dolor sit amet consectuer</h3>
    									<p class="text-left">Lorem ipsum dolor sit amet consectuer lorem</p>
									</div>
								</div>
								<div class="item">
									<img src="http://lorempixel.com/1024/500/people/2" alt="...">
									<div class="carousel-caption">
										<h3 class="text-left">Lorem ipsum dolor sit amet consectuer</h3>
    									<p class="text-left">Lorem ipsum dolor sit amet consectuer lorem</p>
									</div>
								</div>
								<div class="item">
									<img src="http://lorempixel.com/1024/500/people/3" alt="...">
									<div class="carousel-caption">
										<h3 class="text-left">Lorem ipsum dolor sit amet consectuer</h3>
    									<p class="text-left">Lorem ipsum dolor sit amet consectuer lorem</p>
									</div>
								</div>
								<div class="item">
									<img src="http://lorempixel.com/1024/500/people/4" alt="...">
									<div class="carousel-caption">
										<h3 class="text-left">Lorem ipsum dolor sit amet consectuer</h3>
    									<p class="text-left">Lorem ipsum dolor sit amet consectuer lorem</p>
									</div>
								</div>
							</div>

							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-home" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>

						<!-- Ad -->
						<div class="row">
							<div class="text-center ad">
								<img src="https://placehold.it/728x90">
							</div>
						</div>

						<!-- Últimos cursos -->
						<div class="row last-content-list">
							<div class="col-md-12">
								<h2 class="title"><?php _e( 'Últimos cursos', 'colegiovirtual' ); ?></h2>
							</div>
							<div class="col-md-6 last-content-item">
								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object" src="http://lorempixel.com/150/150/nature/3" alt="...">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Media heading</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tempor, lacus et porttitor tincidunt, urna tellus gravida dui, quis volutpat eros orci eu purus.</p>
									</div>
									<a href="#" class="read-more"><?php _e( 'Leer más', 'colegiovirtual' ); ?></a>
								</div>
							</div>
							<div class="col-md-6 last-content-item">
								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object" src="http://lorempixel.com/150/150/nature/3" alt="...">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Media heading</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tempor, lacus et porttitor tincidunt, urna tellus gravida dui, quis volutpat eros orci eu purus.</p>
									</div>
									<a href="#" class="read-more"><?php _e( 'Leer más', 'colegiovirtual' ); ?></a>
								</div>
							</div>
							<div class="col-md-6 last-content-item">
								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object" src="http://lorempixel.com/150/150/nature/3" alt="...">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Media heading</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tempor, lacus et porttitor tincidunt, urna tellus gravida dui, quis volutpat eros orci eu purus.</p>
									</div>
									<a href="#" class="read-more"><?php _e( 'Leer más', 'colegiovirtual' ); ?></a>
								</div>
							</div>
							<div class="col-md-6 last-content-item">
								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object" src="http://lorempixel.com/150/150/nature/3" alt="...">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Media heading</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tempor, lacus et porttitor tincidunt, urna tellus gravida dui, quis volutpat eros orci eu purus.</p>
									</div>
									<a href="#" class="read-more"><?php _e( 'Leer más', 'colegiovirtual' ); ?></a>
								</div>
							</div>
						</div>

						<!-- Últimos ejercicios -->
						<div class="row">
							<div class="col-md-12">
								<h2 class="title"><?php _e( 'Últimos videos', 'colegiovirtual' ); ?></h2>

								<div class="carousel-video-list">
									<div class="carousel-video">
										<a data-fancybox href="https://www.youtube.com/watch?v=_sI_Ps7JSEk&autoplay=1&rel=0&controls=0&showinfo=0" class="watch-more">
											<img src="https://placehold.it/600x600/">
										</a>
									</div>
									<div class="carousel-video">
										<a href="#" class="watch-more">
											<img src="https://placehold.it/600x600/">
										</a>
									</div>
									<div class="carousel-video">
										<a data-fancybox href="https://www.youtube.com/watch?v=_sI_Ps7JSEk&autoplay=1&rel=0&controls=0&showinfo=0" class="watch-more">
											<img src="https://placehold.it/600x600/">
										</a>
									</div>
									<div class="carousel-video">
										<a data-fancybox href="https://www.youtube.com/watch?v=_sI_Ps7JSEk&autoplay=1&rel=0&controls=0&showinfo=0" class="watch-more">
											<img src="https://placehold.it/600x600/">
										</a>
									</div>
									<div class="carousel-video">
										<a data-fancybox href="https://www.youtube.com/watch?v=_sI_Ps7JSEk&autoplay=1&rel=0&controls=0&showinfo=0" class="watch-more">
											<img src="https://placehold.it/600x600/">
										</a>
									</div>
									<div class="carousel-video">
										<a data-fancybox href="https://www.youtube.com/watch?v=_sI_Ps7JSEk&autoplay=1&rel=0&controls=0&showinfo=0" class="watch-more">
											<img src="https://placehold.it/600x600/">
										</a>
									</div>
								</div>

							</div>
						</div>

						<!-- Ad -->
						<div class="row">
							<div class="text-center ad">
								<img src="https://placehold.it/728x90">
							</div>
						</div>

						<!-- Últimas noticias -->
						<div class="row last-content-list">
							<div class="col-md-12">
								<h2 class="title"><?php _e( 'Últimas noticias', 'colegiovirtual' ); ?></h2>
							</div>
							<div class="col-md-6 last-content-item">
								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object" src="http://lorempixel.com/150/150/nature/3" alt="...">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Media heading</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tempor, lacus et porttitor tincidunt, urna tellus gravida dui, quis volutpat eros orci eu purus.</p>
									</div>
									<a href="#" class="read-more"><?php _e( 'Leer más', 'colegiovirtual' ); ?></a>
								</div>
							</div>
							<div class="col-md-6 last-content-item">
								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object" src="http://lorempixel.com/150/150/nature/3" alt="...">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Media heading</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tempor, lacus et porttitor tincidunt, urna tellus gravida dui, quis volutpat eros orci eu purus.</p>
									</div>
									<a href="#" class="read-more"><?php _e( 'Leer más', 'colegiovirtual' ); ?></a>
								</div>
							</div>
							<div class="col-md-6 last-content-item">
								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object" src="http://lorempixel.com/150/150/nature/3" alt="...">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Media heading</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tempor, lacus et porttitor tincidunt, urna tellus gravida dui, quis volutpat eros orci eu purus.</p>
									</div>
									<a href="#" class="read-more"><?php _e( 'Leer más', 'colegiovirtual' ); ?></a>
								</div>
							</div>
							<div class="col-md-6 last-content-item">
								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object" src="http://lorempixel.com/150/150/nature/3" alt="...">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Media heading</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tempor, lacus et porttitor tincidunt, urna tellus gravida dui, quis volutpat eros orci eu purus.</p>
									</div>
									<a href="#" class="read-more"><?php _e( 'Leer más', 'colegiovirtual' ); ?></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<?php get_sidebar(); ?>
					</div>
				</div>

			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

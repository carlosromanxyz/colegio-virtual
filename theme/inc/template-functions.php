<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Colegio_Virtual
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function colegiovirtual_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'colegiovirtual_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function colegiovirtual_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'colegiovirtual_pingback_header' );

/**
 * WooCommerce support
 */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

/**
 * Sensei Support for WooCommerce
 */
add_action( 'after_setup_theme', 'declare_sensei_support' );
function declare_sensei_support() {
    add_theme_support( 'sensei' );
}

/**
 * Sensei Disable default CSS
 */
add_filter( 'sensei_disable_styles', '__return_true' );
